# Reload zsh config
alias reload='. ~/.zshrc && echo '\''Your zshrc file has been reloaded'\'

# git super clean
alias gsc='git gc && git pull && git remote update && git fetch -va'

alias lf='ls -lF'
alias pf='ps aux'
alias pt='ps -o etime= -p'
alias dusum='du -h --max-depth 1'
alias playvideo='xdg-open'
alias imgview='feh --scale-down --auto-zoom'

# https://www.google.com/linuxrepositories/
alias fixchromegpg='wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -'

# Work
alias vpnconnect='sudo openvpn /etc/openvpn/client/client.ovpn'
alias vpndisconnect='sudo killall openvpn'
alias dnsredhat='sudo cp ~/Code/scratch/openvpn/RH-resolv.conf /etc/resolv.conf'
alias dnsregular='sudo cp ~/Code/scratch/openvpn/REGULAR-resolv.conf /etc/resolv.conf'
alias dnspublic='sudo cp ~/Code/scratch/openvpn/PUBLIC-resolv.conf /etc/resolv.conf'
alias kerb='kinit dpeacock@IPA.REDHAT.COM'

# External monitor
alias extmon='xrandr --output HDMI-1 --auto --right-of eDP-1'
alias noextmon='xrandr --output HDMI-1 --off'

# Go pathing
export PATH="/home/dpeacock/go/bin:/usr/local/go/bin:$PATH"

# pyenv items
export PATH="/home/dpeacock/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# spin up temporary python virtualenv
# usage: `tmpenv [python version]`
# defaults to 3.6.6
tmpenv () {
        venv_name="tmpenv-$(cat /dev/urandom | tr -dc '0-9a-zA-Z' | head -c15)" 
        pyenv virtualenv "${1:-3.6.6}" ${venv_name}
        pyenv activate $venv_name
}
# list tmpenvs
lstmpenv () {
    pyenv virtualenvs
}
# tear down all tmpenvs
rmtmpenv () {
        find ~/.pyenv/versions -name 'tmpenv-*' | xargs rm -rf
}
